package com.yubb.generator.service.impl;

import java.util.Arrays;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.generator.domain.dto.GenTableColumnDTO;
import com.yubb.generator.domain.vo.GenTableColumnVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yubb.common.core.text.Convert;
import com.yubb.generator.domain.GenTableColumn;
import com.yubb.generator.mapper.GenTableColumnMapper;
import com.yubb.generator.service.IGenTableColumnService;

/**
 *@Description 业务字段 服务层实现
 *@Author zhushuyong
 *@Date 2021/6/23 17:53
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class GenTableColumnServiceImpl extends
        ServiceImpl<GenTableColumnMapper, GenTableColumn> implements IGenTableColumnService {
    @Autowired
    private GenTableColumnMapper genTableColumnMapper;

    /**
     * 查询业务字段列表
     * 
     * @param genTableColumn 业务字段信息
     * @return 业务字段集合
     */
    @Override
    public List<GenTableColumnVO> selectGenTableColumnListByTableId(GenTableColumnDTO genTableColumn)
    {
        return genTableColumnMapper.selectGenTableColumnListByTableId(genTableColumn);
    }

    /**
     * 新增业务字段
     * 
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    @Override
    public int insertGenTableColumn(GenTableColumnDTO genTableColumn)
    {
        return genTableColumnMapper.insert(DozerUtils.copyProperties(genTableColumn, GenTableColumn.class));
    }

    /**
     * 修改业务字段
     * 
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    @Override
    public int updateGenTableColumn(GenTableColumnDTO genTableColumn)
    {
        return genTableColumnMapper.updateById(DozerUtils.copyProperties(genTableColumn, GenTableColumn.class));
    }

    /**
     * 删除业务字段对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGenTableColumnByIds(String ids)
    {
        return genTableColumnMapper.delete(new LambdaQueryWrapper<GenTableColumn>()
                .in(GenTableColumn::getTableId, Arrays.asList(Convert.toStrArray(ids))));
    }
}