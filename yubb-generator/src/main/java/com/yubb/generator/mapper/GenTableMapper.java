package com.yubb.generator.mapper;

import java.util.List;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.generator.domain.GenTable;
import com.yubb.generator.domain.dto.GenTableDTO;
import com.yubb.generator.domain.vo.GenTableVO;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 业务 数据层
 *@Author zhushuyong
 *@Date 2021/6/23 17:50
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
@InterceptorIgnore(tenantLine = "true")
public interface GenTableMapper extends BaseMapper<GenTable> {
    /**
     * 查询业务列表
     * 
     * @param genTable 业务信息
     * @return 业务集合
     */
    public List<GenTableVO> selectGenTableList(GenTableDTO genTable);

    /**
     * 查询据库列表
     * 
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    public List<GenTableVO> selectDbTableList(GenTableDTO genTable);

    /**
     * 查询据库列表
     * 
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    public List<GenTableVO> selectDbTableListByNames(String[] tableNames);

    /**
     * 查询所有表信息
     * 
     * @return 表信息集合
     */
    public List<GenTableVO> selectGenTableAll();

    /**
     * 查询表ID业务信息
     * 
     * @param id 业务ID
     * @return 业务信息
     */
    public GenTableVO selectGenTableById(String id);

    /**
     * 查询表名称业务信息
     * 
     * @param tableName 表名称
     * @return 业务信息
     */
    public GenTableVO selectGenTableByName(String tableName);

}