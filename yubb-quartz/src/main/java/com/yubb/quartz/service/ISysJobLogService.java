package com.yubb.quartz.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.quartz.domain.SysJobLog;
import com.yubb.quartz.domain.dto.SysJobLogDTO;
import com.yubb.quartz.domain.vo.SysJobLogVO;

/**
 *@Description 定时任务调度日志信息信息 服务层
 *@Author zhushuyong
 *@Date 2021/6/23 23:23
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysJobLogService extends IService<SysJobLog>
{
    /**
     * 获取quartz调度器日志的计划任务
     * 
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    public List<SysJobLogVO> selectJobLogList(SysJobLogDTO jobLog);

    /**
     * 通过调度任务日志ID查询调度信息
     * 
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    public SysJobLogVO selectJobLogById(String jobLogId);

    /**
     * 新增任务日志
     * 
     * @param jobLog 调度日志信息
     */
    public void addJobLog(SysJobLogDTO jobLog);

    /**
     * 批量删除调度日志信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJobLogByIds(String ids);

    /**
     * 删除任务日志
     * 
     * @param jobId 调度日志ID
     * @return 结果
     */
    public int deleteJobLogById(String jobId);
    
    /**
     * 清空任务日志
     */
    public void cleanJobLog();
}
