package com.yubb.quartz.task;

import org.springframework.stereotype.Component;
import com.yubb.common.utils.StringUtils;

/**
 *@Description 定时任务调度测试
 *@Author zhushuyong
 *@Date 2021/6/24 9:25
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Component("ybTask")
public class YbTask
{
    public void ybMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ybParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ybNoParams()
    {
        System.out.println("执行无参方法");
    }
}
