package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.platform.domain.PlatformUserRole;
import com.yubb.platform.mapper.PlatformUserRoleMapper;
import com.yubb.platform.service.IPlatformUserRoleService;
import org.springframework.stereotype.Service;

/**
 * @Description 用户角色业务接口实现
 * @Author zhushuyong
 * @Date 2021/6/16 22:22
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class PlatformUserRoleServiceImpl extends ServiceImpl<PlatformUserRoleMapper, PlatformUserRole> implements IPlatformUserRoleService {

}
