package com.yubb.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.platform.PlatformUser;
import com.yubb.common.core.domain.platform.dto.PlatformUserDTO;
import com.yubb.common.core.domain.platform.vo.PlatformUserVO;
import java.util.List;

/**
 * @Description 平台用户接口
 * @Author zhushuyong
 * @Date 2021/7/15 11:03
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface IPlatformUserService extends IService<PlatformUser> {

    /**
     * 根据条件分页查询用户列表
     *
     * @param userDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<PlatformUserVO> selectUserList(PlatformUserDTO userDTO);

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param userDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<PlatformUserVO> selectAllocatedList(PlatformUserDTO userDTO);

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param userDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<PlatformUserVO> selectUnallocatedList(PlatformUserDTO userDTO);

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public PlatformUserVO selectUserById(String userId);

    /**
     * 通过登录用户名查询用户
     * @param loginName 登录名
     * @return
     */
    public PlatformUserVO selectUserByLoginName(String loginName);

    /**
     * 批量删除用户信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deleteUserByIds(String ids);

    /**
     * 保存用户信息
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public int insertUser(PlatformUserDTO userDTO);

    /**
     * 注册用户信息
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public boolean registerUser(PlatformUserDTO userDTO);

    /**
     * 保存用户信息
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public int updateUser(PlatformUserDTO userDTO);

    /**
     * 修改用户详细信息
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public int updateUserInfo(PlatformUserDTO userDTO);

    /**
     * 用户授权角色
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    public void insertUserAuth(String userId, String[] roleIds);

    /**
     * 修改用户密码信息
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public int resetUserPwd(PlatformUserDTO userDTO);

    /**
     * 校验用户名称是否唯一
     *
     * @param loginName 登录名称
     * @return 结果
     */
    public String checkLoginNameUnique(String loginName);

    /**
     * 校验手机号码是否唯一
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public String checkPhoneUnique(PlatformUserDTO userDTO);

    /**
     * 校验email是否唯一
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public String checkEmailUnique(PlatformUserDTO userDTO);

    /**
     * 校验用户是否允许操作
     *
     * @param userDTO 用户信息
     */
    public void checkUserAllowed(PlatformUserDTO userDTO);

    /**
     * 根据用户ID查询用户所属角色组
     *
     * @param userId 用户ID
     * @return 结果
     */
    public String selectUserRoleGroup(String userId);

    /**
     * 根据用户ID查询用户所属岗位组
     *
     * @param userId 用户ID
     * @return 结果
     */
    public String selectUserPostGroup(String userId);

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<PlatformUserDTO> userList, Boolean isUpdateSupport, String operName);

    /**
     * 用户状态修改
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public int changeStatus(PlatformUserDTO userDTO);
    
}
