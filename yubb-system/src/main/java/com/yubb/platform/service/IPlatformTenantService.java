package com.yubb.platform.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.platform.PlatformTenant;
import com.yubb.common.core.domain.platform.dto.PlatformTenantDTO;
import com.yubb.common.core.domain.platform.dto.PlatformUserDTO;
import com.yubb.common.core.domain.platform.vo.PlatformTenantVO;
import com.yubb.common.core.page.TableDataInfo;

/**
 * 租户Service接口
 * 
 * @author zhushuyong
 * @date 2021-07-02
 */
public interface IPlatformTenantService extends IService<PlatformTenant>
{
    /**
     * 查询租户
     * 
     * @param id 租户ID
     * @return 租户
     */
    public PlatformTenantVO selectPlatformTenantById(String id);

    /**
     * 查询租户列表
     * 
     * @param platformTenant 租户
     * @return 租户集合
     */
    public TableDataInfo<PlatformTenantVO> selectPlatformTenantPage(PlatformTenantDTO platformTenant);

    /**
     * 查询租户列表
     *
     * @param platformTenant 租户
     * @return 租户集合
     */
    public List<PlatformTenantVO> selectPlatformTenantList(PlatformTenantDTO platformTenant);

    /**
     * 新增租户
     * 
     * @param platformTenant 租户
     * @return 结果
     */
    public int insertPlatformTenant(PlatformTenantDTO platformTenant);

    /**
     * 修改租户
     * 
     * @param platformTenant 租户
     * @return 结果
     */
    public int updatePlatformTenant(PlatformTenantDTO platformTenant);

    /**
     * 批量删除租户
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatformTenantByIds(String ids);

    /**
     * 删除租户信息
     * 
     * @param id 租户ID
     * @return 结果
     */
    public int deletePlatformTenantById(String id);

    /**
     * 校验租户编号是否唯一
     * @param platformTenantDTO
     * @return
     */
    public String checkTenantNoUnique(PlatformTenantDTO platformTenantDTO);

    /**
     * 租户状态修改
     *
     * @param platformTenantDTO
     * @return 结果
     */
    public int changeStatus(PlatformTenantDTO platformTenantDTO);

}
