package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.annotation.DataScope;
import com.yubb.common.annotation.PlatformDataScope;
import com.yubb.common.constant.Constants;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.domain.platform.PlatformRole;
import com.yubb.common.core.domain.platform.dto.PlatformRoleDTO;
import com.yubb.common.core.domain.platform.vo.PlatformRoleVO;
import com.yubb.common.core.text.Convert;
import com.yubb.common.exception.BusinessException;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.common.utils.spring.SpringUtils;
import com.yubb.platform.domain.PlatformRoleDept;
import com.yubb.platform.domain.PlatformRoleMenu;
import com.yubb.platform.domain.PlatformUserRole;
import com.yubb.platform.mapper.PlatformRoleMapper;
import com.yubb.platform.service.IPlatformRoleDeptService;
import com.yubb.platform.service.IPlatformRoleMenuService;
import com.yubb.platform.service.IPlatformRoleService;
import com.yubb.platform.service.IPlatformUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Description 平台角色接口实现
 * @Author zhushuyong
 * @Date 2021/7/15 22:54
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Slf4j
@Service
public class PlatformRoleServiceImpl extends ServiceImpl<PlatformRoleMapper, PlatformRole> implements IPlatformRoleService {

    @Resource
    private PlatformRoleMapper roleMapper;

    @Resource
    private IPlatformRoleMenuService roleMenuService;

    @Resource
    private IPlatformRoleDeptService roleDeptService;

    @Resource
    private IPlatformUserRoleService userRoleService;

    /**
     * 根据条件分页查询角色数据
     *
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    @Override
    @PlatformDataScope(deptAlias = "d")
    public List<PlatformRoleVO> selectRoleList(PlatformRoleDTO roleDTO) {
        return roleMapper.selectRoleList(roleDTO);
    }

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户
     * @return 权限列表
     */
    @Override
    public Set<String> selectRoleKeys(String userId) {
        List<PlatformRoleVO> perms = roleMapper.selectRolesByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (PlatformRoleVO perm : perms) {
            if (StringUtils.isNotNull(perm)) {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户
     * @return 角色列表
     */
    @Override
    public List<PlatformRoleVO> selectRolesByUserId(String userId) {
        List<PlatformRoleVO> userRoles = roleMapper.selectRolesByUserId(userId);
        List<PlatformRoleVO> roles = selectRoleAll();
        for (PlatformRoleVO role : roles) {
            for (PlatformRoleVO userRole : userRoles) {
                if (role.getId().equals(userRole.getId())) {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }

    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    @Override
    public List<PlatformRoleVO> selectRoleAll() {
        return SpringUtils.getAopProxy(this).selectRoleList(new PlatformRoleDTO());
    }

    /**
     * 通过角色ID查询角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    @Override
    public PlatformRoleVO selectRoleById(String roleId) {
        return DozerUtils.copyProperties(roleMapper.selectOne(Wrappers.query(PlatformRole.builder()
                .id(roleId)
                .delFlag(Constants.DEL_FLAG_NORMAL)
                .build())),PlatformRoleVO.class);
    }

    /**
     * 通过角色ID删除角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    @Transactional
    public boolean deleteRoleById(String roleId) {
        // 删除角色与菜单关联
        roleMenuService.remove(Wrappers.query(PlatformRoleMenu.builder().roleId(roleId).build()));
        // 删除角色与部门关联
        roleDeptService.remove(Wrappers.query(PlatformRoleDept.builder().roleId(roleId).build()));
        return roleMapper.update(PlatformRole.builder().delFlag(Constants.DEL_FLAG_DEL).build(),
                Wrappers.query(PlatformRole.builder().id(roleId).build())) > 0 ? true : false;
    }

    /**
     * 批量删除角色信息
     *
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    @Override
    @Transactional
    public int deleteRoleByIds(String ids) {
        String[] roleIds = Convert.toStrArray(ids);
        for (String roleId : roleIds) {
            checkRoleAllowed(PlatformRoleDTO.builder().id(roleId).build());
            PlatformRoleVO role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0) {
                throw new BusinessException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        // 删除角色与菜单关联
        roleMenuService.remove(new LambdaQueryWrapper<PlatformRoleMenu>().in(PlatformRoleMenu::getRoleId, Arrays.asList(roleIds)));
        // 删除角色与部门关联
        roleDeptService.remove(new LambdaQueryWrapper<PlatformRoleDept>().in(PlatformRoleDept::getRoleId, Arrays.asList(roleIds)));
        return roleMapper.deleteBatchIds(Arrays.asList(roleIds));
    }

    /**
     * 新增保存角色信息
     *
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean insertRole(PlatformRoleDTO roleDTO) {
        // 新增角色信息
        PlatformRole platformRole = DozerUtils.copyProperties(roleDTO, PlatformRole.class);
        roleMapper.insert(platformRole);
        roleDTO.setId(platformRole.getId());
        return insertRoleMenu(roleDTO);
    }

    /**
     * 修改保存角色信息
     *
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean updateRole(PlatformRoleDTO roleDTO) {
        // 修改角色信息
        roleMapper.updateById(DozerUtils.copyProperties(roleDTO, PlatformRole.class));
        // 删除角色与菜单关联
        roleMenuService.remove(Wrappers.query(PlatformRoleMenu.builder().roleId(roleDTO.getId()).build()));
        return insertRoleMenu(roleDTO);
    }

    /**
     * 修改数据权限信息
     *
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean authDataScope(PlatformRoleDTO roleDTO) {
        // 修改角色信息
        roleMapper.updateById(DozerUtils.copyProperties(roleDTO, PlatformRole.class));
        // 删除角色与部门关联
        roleDeptService.remove(Wrappers.query(PlatformRoleDept.builder().roleId(roleDTO.getId()).build()));
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(roleDTO);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public boolean insertRoleMenu(PlatformRoleDTO role) {
        boolean rows = true;
        // 新增用户与角色管理
        List<PlatformRoleMenu> list = new ArrayList<PlatformRoleMenu>();
        for (String menuId : role.getMenuIds()) {
            list.add(PlatformRoleMenu.builder().roleId(role.getId()).menuId(menuId).build());
        }
        if (list.size() > 0) {
            rows = roleMenuService.saveBatch(list);
        }
        return rows;
    }

    /**
     * 新增角色部门信息(数据权限)
     *
     * @param roleDTO 角色对象
     */
    public boolean insertRoleDept(PlatformRoleDTO roleDTO) {
        boolean rows = true;
        // 新增角色与部门（数据权限）管理
        List<PlatformRoleDept> list = new ArrayList<PlatformRoleDept>();
        for (String deptId : roleDTO.getDeptIds()) {
            list.add(PlatformRoleDept.builder().roleId(roleDTO.getId()).deptId(deptId).build());
        }
        if (list.size() > 0) {
            rows = roleDeptService.saveBatch(list);
        }
        return rows;
    }

    /**
     * 校验角色名称是否唯一
     *
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleNameUnique(PlatformRoleDTO roleDTO) {
        String roleId = StringUtils.isIdNull(roleDTO.getId());
        Optional<PlatformRole> info = roleMapper.selectList(Wrappers.query(PlatformRole.builder()
                .roleName(roleDTO.getRoleName())
                .build())).stream().findFirst();
        if (info.isPresent() && !roleId.equals(info.get().getId())) {
            return UserConstants.ROLE_NAME_NOT_UNIQUE;
        }
        return UserConstants.ROLE_NAME_UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     *
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleKeyUnique(PlatformRoleDTO roleDTO) {
        String roleId = StringUtils.isIdNull(roleDTO.getId());
        Optional<PlatformRole> info = roleMapper.selectList(Wrappers.query(PlatformRole.builder()
                .roleKey(roleDTO.getRoleKey())
                .build())).stream().findFirst();
        if (info.isPresent() && !roleId.equals(info.get().getId())) {
            return UserConstants.ROLE_KEY_NOT_UNIQUE;
        }
        return UserConstants.ROLE_KEY_UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     *
     * @param roleDTO 角色信息
     */
    @Override
    public void checkRoleAllowed(PlatformRoleDTO roleDTO) {
        if (StringUtils.isNotBlank(roleDTO.getId()) && this.getById(roleDTO.getId()).isAdmin()) {
            throw new BusinessException("不允许操作超级管理员角色");
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public long countUserRoleByRoleId(String roleId) {
        return userRoleService.count(Wrappers.query(PlatformUserRole.builder().roleId(roleId).build()));
    }

    /**
     * 角色状态修改
     *
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    public int changeStatus(PlatformRoleDTO roleDTO) {
        return roleMapper.updateById(DozerUtils.copyProperties(roleDTO,PlatformRole.class));
    }

    /**
     * 取消授权用户角色
     *
     * @param userRoleDTO 用户和角色关联信息
     * @return 结果
     */
    @Override
    public boolean deleteAuthUser(PlatformUserRole userRoleDTO) {
        return userRoleService.remove(Wrappers.query(PlatformUserRole.builder()
                .roleId(userRoleDTO.getRoleId())
                .userId(userRoleDTO.getUserId())
                .build()));
    }

    /**
     * 批量取消授权用户角色
     *
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    @Override
    public boolean deleteAuthUsers(String roleId, String userIds) {
        return userRoleService.remove(new LambdaQueryWrapper<PlatformUserRole>()
                .eq(PlatformUserRole::getRoleId, roleId)
                .in(PlatformUserRole::getUserId, Arrays.asList(Convert.toStrArray(userIds))));
    }

    /**
     * 批量选择授权用户角色
     *
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    @Override
    public boolean insertAuthUsers(String roleId, String userIds) {
        boolean row = true;
        String[] users = Convert.toStrArray(userIds);
        // 新增用户与角色管理
        List<PlatformUserRole> list = new ArrayList<PlatformUserRole>();
        for (String userId : users) {
            list.add(PlatformUserRole.builder().userId(userId).roleId(roleId).build());
        }
        if (list.size()>0) {
            row = userRoleService.saveBatch(list);
        }
        return row;
    }

}
