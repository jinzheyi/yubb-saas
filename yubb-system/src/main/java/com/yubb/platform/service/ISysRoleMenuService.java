package com.yubb.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.platform.domain.SysRoleMenu;

/**
 * @Description 角色菜单关联接口
 * @Author zhushuyong
 * @Date 2021/6/19 15:58
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {
}
