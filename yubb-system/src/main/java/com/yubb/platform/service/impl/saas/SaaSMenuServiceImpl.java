package com.yubb.platform.service.impl.saas;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.domain.Ztree;
import com.yubb.common.core.domain.platform.SysMenu;
import com.yubb.common.core.domain.platform.dto.SysMenuDTO;
import com.yubb.common.core.domain.platform.vo.SysMenuVO;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.entity.SysRole;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.platform.domain.SysRoleMenu;
import com.yubb.platform.mapper.SysRoleMenuMapper;
import com.yubb.platform.mapper.saas.SaaSMenuMapper;
import com.yubb.platform.mapper.saas.SaaSRoleMapper;
import com.yubb.platform.service.saas.ISaaSMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 *@Description 菜单 业务层处理
 *@Author zhushuyong
 *@Date 2021/6/21 17:31
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SaaSMenuServiceImpl extends ServiceImpl<SaaSMenuMapper, SysMenu> implements ISaaSMenuService
{

    @Autowired
    private SaaSMenuMapper saaSMenuMapper;

    @Autowired
    private SysRoleMenuMapper roleMenuMapper;

    @Autowired
    private SaaSRoleMapper saaSRoleMapper;

    /**
     * 查询菜单集合
     * 
     * @return 所有菜单信息
     */
    @Override
    public List<SysMenuVO> selectMenuList(SysMenuDTO menu)
    {
        return saaSMenuMapper.selectMenuList(menu);
    }

    /**
     * 查询所有菜单
     * 
     * @return 菜单列表
     */
    @Override
    public List<Ztree> menuTreeData()
    {
        List<SysMenuVO> menuList = saaSMenuMapper.selectMenuAll();
        List<Ztree> ztrees = initZtree(menuList);
        return ztrees;
    }

    /**
     * 对象转菜单树
     * 
     * @param menuList 菜单列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<SysMenuVO> menuList)
    {
        return initZtree(menuList, null, false);
    }

    /**
     * 对象转菜单树
     * 
     * @param menuList 菜单列表
     * @param roleMenuList 角色已存在菜单列表
     * @param permsFlag 是否需要显示权限标识
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<SysMenuVO> menuList, List<String> roleMenuList, boolean permsFlag)
    {
        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(roleMenuList);
        for (SysMenuVO menu : menuList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(menu.getId());
            ztree.setpId(menu.getParentId());
            ztree.setName(transMenuName(menu, permsFlag));
            ztree.setTitle(menu.getMenuName());
            if (isCheck)
            {
                ztree.setChecked(roleMenuList.contains(menu.getId() + menu.getPerms()));
            }
            ztrees.add(ztree);
        }
        return ztrees;
    }

    public String transMenuName(SysMenuVO menu, boolean permsFlag)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(menu.getMenuName());
        if (permsFlag)
        {
            sb.append("<font color=\"#888\">&nbsp;&nbsp;&nbsp;" + menu.getPerms() + "</font>");
        }
        return sb.toString();
    }

    /**
     * 删除菜单管理信息
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    @Override
    public int deleteMenuById(String menuId)
    {
        return saaSMenuMapper.deleteMenuById(menuId);
    }

    /**
     * 根据菜单ID查询信息
     * 
     * @param menuId 菜单ID
     * @return 菜单信息
     */
    @Override
    public SysMenuVO selectMenuById(String menuId)
    {
        return saaSMenuMapper.selectMenuById(menuId);
    }

    /**
     * 查询子菜单数量
     * 
     * @param parentId 父级菜单ID
     * @return 结果
     */
    @Override
    public long selectCountMenuByParentId(String parentId)
    {
        return saaSMenuMapper.selectCount(Wrappers.query(SysMenu.builder().parentId(parentId).build()));
    }

    /**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    @Override
    public long selectCountRoleMenuByMenuId(String menuId)
    {
        return roleMenuMapper.selectCount(Wrappers.query(SysRoleMenu.builder().menuId(menuId).build()));
    }

    /**
     * 新增保存菜单信息
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    @Override
    public int insertMenu(SysMenuDTO menu)
    {
        menu.setMenuCType("00");
        return saaSMenuMapper.insert(DozerUtils.copyProperties(menu, SysMenu.class));
    }

    /**
     * 修改保存菜单信息
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    @Override
    public int updateMenu(SysMenuDTO menu)
    {
        return saaSMenuMapper.updateById(DozerUtils.copyProperties(menu, SysMenu.class));
    }

    /**
     * 校验菜单名称是否唯一
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    @Override
    public String checkMenuNameUnique(SysMenuDTO menu)
    {
        String menuId = StringUtils.isIdNull(menu.getId());
        Optional<SysMenu> info = saaSMenuMapper.selectList(Wrappers.query(SysMenu.builder()
                .menuName(menu.getMenuName()).parentId(menu.getParentId()).build())).stream().findFirst();
        if (info.isPresent() && !menuId.equals(info.get().getId()))
        {
            return UserConstants.MENU_NAME_NOT_UNIQUE;
        }
        return UserConstants.MENU_NAME_UNIQUE;
    }

    /**
     * 根据角色ID查询菜单
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    @Override
    public List<Ztree> roleMenuTreeData(SysRoleDTO role, boolean permsFlag)
    {
        String roleId = role.getId();
        List<Ztree> ztrees = new ArrayList<Ztree>();
        List<SysMenuVO> menuList = saaSMenuMapper.selectMenuTenantAll();
        if (StringUtils.isNotNull(roleId))
        {
            SysRole sysRole = saaSRoleMapper.selectById(roleId);
            //租户超管角色
            if (!Objects.isNull(sysRole) && sysRole.isAdmin()) {
                menuList = saaSMenuMapper.selectMenuTenantAdminAll();
            }
            List<String> roleMenuList = saaSMenuMapper.selectMenuTree(roleId);
            ztrees = initZtree(menuList, roleMenuList, permsFlag);
        }
        else
        {
            ztrees = initZtree(menuList, null, permsFlag);
        }
        return ztrees;
    }

}
