package com.yubb.platform.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.platform.dto.SysDictDataDTO;
import com.yubb.common.core.domain.platform.SysDictData;
import com.yubb.common.core.domain.platform.vo.SysDictDataVO;

/**
 *@Description 字典 业务层
 *@Author zhushuyong
 *@Date 2021/6/21 14:31
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysDictDataService extends IService<SysDictData> {
    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    public List<SysDictDataVO> selectDictDataList(SysDictDataDTO dictData);

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    public String selectDictLabel(String dictType, String dictValue);

    /**
     * 根据字典数据ID查询信息
     * 
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    public SysDictDataVO selectDictDataById(String dictCode);

    /**
     * 批量删除字典数据
     * 
     * @param ids 需要删除的数据
     * @return 结果
     */
    public int deleteDictDataByIds(String ids);

    /**
     * 新增保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    public int insertDictData(SysDictDataDTO dictData);

    /**
     * 修改保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    public int updateDictData(SysDictDataDTO dictData);
}
