package com.yubb.platform.service.saas;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.Ztree;
import com.yubb.common.core.domain.platform.SysMenu;
import com.yubb.common.core.domain.platform.dto.SysMenuDTO;
import com.yubb.common.core.domain.platform.vo.SysMenuVO;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;

import java.util.List;

/**
 *@Description 菜单 业务层
 *@Author zhushuyong
 *@Date 2021/6/21 22:27
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISaaSMenuService extends IService<SysMenu> {

    /**
     * 查询系统菜单列表
     * 
     * @param menu 菜单信息
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenuList(SysMenuDTO menu);

    /**
     * 查询所有菜单信息
     *
     * @return 菜单列表
     */
    public List<Ztree> menuTreeData();

    /**
     * 删除菜单管理信息
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    public int deleteMenuById(String menuId);

    /**
     * 根据菜单ID查询信息
     * 
     * @param menuId 菜单ID
     * @return 菜单信息
     */
    public SysMenuVO selectMenuById(String menuId);

    /**
     * 查询菜单数量
     * 
     * @param parentId 菜单父ID
     * @return 结果
     */
    public long selectCountMenuByParentId(String parentId);

    /**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    public long selectCountRoleMenuByMenuId(String menuId);

    /**
     * 新增保存菜单信息
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    public int insertMenu(SysMenuDTO menu);

    /**
     * 修改保存菜单信息
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    public int updateMenu(SysMenuDTO menu);

    /**
     * 校验菜单名称是否唯一
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    public String checkMenuNameUnique(SysMenuDTO menu);

    /**
     * 根据角色ID查询租户角色所能拥有的菜单
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    public List<Ztree> roleMenuTreeData(SysRoleDTO role,boolean permsFlag);

}
