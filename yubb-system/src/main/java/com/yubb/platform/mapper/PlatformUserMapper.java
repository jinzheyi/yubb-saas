package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.platform.PlatformUser;
import com.yubb.common.core.domain.platform.dto.PlatformUserDTO;
import com.yubb.common.core.domain.platform.vo.PlatformUserVO;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * @Description 平台用户mapper接口
 * @Author zhushuyong
 * @Date 2021/7/15 11:06
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformUserMapper extends BaseMapper<PlatformUser> {

    /**
     * 根据条件分页查询用户列表
     *
     * @param platformUserDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<PlatformUserVO> selectUserList(PlatformUserDTO platformUserDTO);

    /**
     * 根据条件分页查询未已配用户角色列表
     *
     * @param platformUserDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<PlatformUserVO> selectAllocatedList(PlatformUserDTO platformUserDTO);

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param platformUserDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<PlatformUserVO> selectUnallocatedList(PlatformUserDTO platformUserDTO);

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public PlatformUserVO selectUserById(String userId);

    /**
     * 通过登录用户名查询用户
     * @param loginName 登录名
     * @return
     */
    public PlatformUserVO selectUserByLoginName(String loginName);
    
}
