package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.platform.PlatformRole;
import com.yubb.common.core.domain.platform.dto.PlatformRoleDTO;
import com.yubb.common.core.domain.platform.vo.PlatformRoleVO;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * @Description 平台角色
 * @Author zhushuyong
 * @Date 2021/7/15 22:49
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformRoleMapper extends BaseMapper<PlatformRole> {

    /**
     * 根据条件分页查询角色数据
     *
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    public List<PlatformRoleVO> selectRoleList(PlatformRoleDTO roleDTO);

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<PlatformRoleVO> selectRolesByUserId(String userId);

}
