package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.platform.PlatformMenu;
import com.yubb.common.core.domain.platform.dto.PlatformMenuDTO;
import com.yubb.common.core.domain.platform.vo.PlatformMenuVO;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * @Description 平台菜单mapper
 * @Author zhushuyong
 * @Date 2021/7/15 22:43
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformMenuMapper extends BaseMapper<PlatformMenu> {

    /**
     * 查询系统所有菜单（含按钮）
     *
     * @return 菜单列表
     */
    public List<PlatformMenuVO> selectMenuAll();

    /**
     * 根据用户ID查询菜单
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<PlatformMenuVO> selectMenuAllByUserId(String userId);

    /**
     * 查询系统正常显示菜单（不含按钮）
     *
     * @return 菜单列表
     */
    public List<PlatformMenuVO> selectMenuNormalAll();

    /**
     * 根据用户ID查询菜单（不含按钮）
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<PlatformMenuVO> selectMenusByUserId(String userId);

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    public List<String> selectPermsByUserId(String userId);

    /**
     * 根据角色ID查询菜单
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    public List<String> selectMenuTree(String roleId);

    /**
     * 查询系统菜单列表
     *
     * @param menu 菜单信息
     * @return 菜单列表
     */
    public List<PlatformMenuVO> selectMenuList(PlatformMenuDTO menu);

    /**
     * 查询系统菜单列表
     *
     * @param menu 菜单信息
     * @return 菜单列表
     */
    public List<PlatformMenuVO> selectMenuListByUserId(PlatformMenuDTO menu);

    /**
     * 删除菜单管理信息
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    public int deleteMenuById(String menuId);

    /**
     * 根据菜单ID查询信息
     *
     * @param menuId 菜单ID
     * @return 菜单信息
     */
    public PlatformMenuVO selectMenuById(String menuId);

}
