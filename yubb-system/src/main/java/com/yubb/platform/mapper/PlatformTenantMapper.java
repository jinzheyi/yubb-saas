package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.platform.PlatformTenant;
import com.yubb.common.core.domain.platform.vo.PlatformTenantVO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 租户Mapper接口
 * 
 * @author zhushuyong
 * @date 2021-07-02
 */
@Mapper
@InterceptorIgnore(tenantLine = "true")
public interface PlatformTenantMapper extends BaseMapper<PlatformTenant>
{

    public PlatformTenantVO selectByTenantId(String id);

}
