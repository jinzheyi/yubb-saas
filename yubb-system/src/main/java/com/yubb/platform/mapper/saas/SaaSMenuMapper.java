package com.yubb.platform.mapper.saas;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.platform.SysMenu;
import com.yubb.common.core.domain.platform.dto.SysMenuDTO;
import com.yubb.common.core.domain.platform.vo.SysMenuVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *@Description 菜单表 数据层
 *@Author zhushuyong
 *@Date 2021/6/20 20:42
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
@InterceptorIgnore(tenantLine = "true")
public interface SaaSMenuMapper extends BaseMapper<SysMenu> {
    /**
     * 查询系统所有菜单（含按钮）
     * 
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenuAll();

    /**
     * 查询系统菜单列表
     * 
     * @param menu 菜单信息
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenuList(SysMenuDTO menu);

    /**
     * 删除菜单管理信息
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    public int deleteMenuById(String menuId);

    /**
     * 根据菜单ID查询信息
     * 
     * @param menuId 菜单ID
     * @return 菜单信息
     */
    public SysMenuVO selectMenuById(String menuId);

    /**
     * 根据角色ID查询菜单
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    public List<String> selectMenuTree(String roleId);

    /**
     * 查询租户角色所能拥有的菜单【基于租户超管角色的菜单总量基础】（含按钮）
     *
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenuTenantAll();

    /**
     * 查询租户超管角色所能拥有的菜单（含按钮）
     *
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenuTenantAdminAll();

    /**
     * 根据用户ID查询菜单（不含按钮）
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenusByUserId(String userId);

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    public List<String> selectPermsByUserId(String userId);

    /**
     * 根据用户ID查询菜单
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenuAllByUserId(String userId);

}
