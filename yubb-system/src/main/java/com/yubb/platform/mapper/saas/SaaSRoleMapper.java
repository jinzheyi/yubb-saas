package com.yubb.platform.mapper.saas;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.entity.SysRole;
import com.yubb.common.core.domain.saas.vo.SysRoleVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *@description 角色表 数据层
 *@author zhushuyong
 *@date 2021/6/9 14:19
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
@InterceptorIgnore(tenantLine = "true")
public interface SaaSRoleMapper extends BaseMapper<SysRole> {
    /**
     * 根据条件分页查询角色数据
     * 
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRoleVO> selectRoleList(SysRoleDTO roleDTO);

    /**
     * 根据条件分页查询角色数据
     *
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRoleVO> selectPfRoleList(SysRoleDTO roleDTO);

    /**
     * 查询超管角色id
     * @return
     */
    public SysRoleVO selectRoleByTenantId();

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<SysRoleVO> selectRolesByUserId(String userId);

}
