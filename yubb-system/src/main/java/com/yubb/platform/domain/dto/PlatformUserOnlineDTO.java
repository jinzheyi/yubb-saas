package com.yubb.platform.domain.dto;

import com.yubb.common.core.domain.platform.base.PlatformDTO;
import com.yubb.common.enums.OnlineStatus;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 *@Description 当前在线会话DTO
 *@Author zhushuyong
 *@Date 2021/6/19 22:05
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PlatformUserOnlineDTO extends PlatformDTO {
    private static final long serialVersionUID = 1L;

    /** 部门名称 */
    private String deptName;

    /** 登录名称 */
    private String loginName;

    /** 登录IP地址 */
    private String ipaddr;

    /** 登录地址 */
    private String loginLocation;

    /** 浏览器类型 */
    private String browser;

    /** 操作系统 */
    private String os;

    /** session创建时间 */
    private Date startTimestamp;

    /** session最后访问时间 */
    private Date lastAccessTime;

    /** 超时时间，单位为分钟 */
    private Long expireTime;

    private String tenantId;

    /** 在线状态 */
    @Builder.Default
    private OnlineStatus status = OnlineStatus.on_line;

}
