package com.yubb.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.entity.SysRole;
import com.yubb.common.core.domain.saas.vo.SysRoleVO;
import com.yubb.common.core.domain.saas.vo.SysUserVO;
import com.yubb.system.domain.SysUserRole;

import java.util.List;
import java.util.Set;

/**
 *@Description 角色业务层
 *@Author zhushuyong
 *@Date 2021/6/6 16:51
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 返回用户角色数据
     * @param user
     * @return
     */
    public List<SysRoleVO> selectRolesByUser(SysUserVO user);

    /**
     * 根据条件分页查询角色数据
     * 
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRoleVO> selectRoleList(SysRoleDTO roleDTO);

    /**
     * 根据用户ID查询角色
     * 
     * @param user 用户
     * @return 权限列表
     */
    public Set<String> selectRoleKeys(SysUserVO user);

    /**
     * 根据用户ID查询角色
     * 
     * @param user 用户
     * @return 角色列表
     */
    public List<SysRoleVO> selectRolesByUserId(SysUserVO user);

    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    public List<SysRoleVO> selectRoleAll();

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public SysRoleVO selectRoleById(String roleId);

    /**
     * 通过角色ID删除角色
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public boolean deleteRoleById(String roleId);

    /**
     * 批量删除角色用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deleteRoleByIds(String ids);

    /**
     * 新增保存角色信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    public boolean insertRole(SysRoleDTO roleDTO);

    /**
     * 修改保存角色信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    public boolean updateRole(SysRoleDTO roleDTO);

    /**
     * 修改数据权限信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public boolean authDataScope(SysRoleDTO role);

    /**
     * 校验角色是否允许操作
     * 
     * @param roleDTO 角色信息
     */
    public void checkRoleAllowed(SysRoleDTO roleDTO);

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public long countUserRoleByRoleId(String roleId);

    /**
     * 角色状态修改
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int changeStatus(SysRoleDTO role);

    /**
     * 取消授权用户角色
     * 
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    public boolean deleteAuthUser(SysUserRole userRole);

    /**
     * 批量取消授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    public boolean deleteAuthUsers(String roleId, String userIds);

    /**
     * 批量选择授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    public boolean insertAuthUsers(String roleId, String userIds);
}
