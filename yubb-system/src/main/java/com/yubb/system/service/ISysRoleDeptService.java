package com.yubb.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.system.domain.SysRoleDept;

/**
 * @Description 角色部门接口
 * @Author zhushuyong
 * @Date 2021/6/19 16:08
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysRoleDeptService extends IService<SysRoleDept> {
}
