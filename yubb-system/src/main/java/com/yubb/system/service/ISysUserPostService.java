package com.yubb.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.system.domain.SysUserPost;

/**
 * @Description 用户岗位接口
 * @Author zhushuyong
 * @Date 2021/6/17 21:25
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysUserPostService extends IService<SysUserPost> {
}
