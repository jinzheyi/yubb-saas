package com.yubb.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.saas.dto.SysUserDTO;
import com.yubb.common.core.domain.saas.entity.SysUser;
import com.yubb.common.core.domain.saas.vo.SysUserVO;

/**
 *@Description 用户业务层
 *@Author zhushuyong
 *@Date 2021/5/31 23:27
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysUserService extends IService<SysUser> {
    /**
     * 根据条件分页查询用户列表
     * 
     * @param userDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUserVO> selectUserList(SysUserDTO userDTO);

    /**
     * 根据条件分页查询已分配用户角色列表
     * 
     * @param userDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUserVO> selectAllocatedList(SysUserDTO userDTO);

    /**
     * 根据条件分页查询未分配用户角色列表
     * 
     * @param userDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUserVO> selectUnallocatedList(SysUserDTO userDTO);

    /**
     * 通过用户名，租户id查询用户
     * @param userName 登录名
     * @param tenantId 租户id
     * @return
     */
    public SysUserVO selectUserByLoginNameAndTenantId(String userName, String tenantId);

    /**
     * 通过用户ID查询用户
     * 
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUserVO selectUserById(String userId);

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deleteUserByIds(String ids);

    /**
     * 保存用户信息
     * 
     * @param userDTO 用户信息
     * @return 结果
     */
    public int insertUser(SysUserDTO userDTO);

    /**
     * 注册用户信息
     * 
     * @param userDTO 用户信息
     * @return 结果
     */
    public boolean registerUser(SysUserDTO userDTO);

    /**
     * 保存用户信息
     * 
     * @param userDTO 用户信息
     * @return 结果
     */
    public int updateUser(SysUserDTO userDTO);

    /**
     * 修改用户详细信息
     * 
     * @param userDTO 用户信息
     * @return 结果
     */
    public int updateUserInfo(SysUserDTO userDTO);

    /**
     * 用户授权角色
     * 
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    public void insertUserAuth(String userId, String[] roleIds);

    /**
     * 修改用户密码信息
     * 
     * @param userDTO 用户信息
     * @return 结果
     */
    public int resetUserPwd(SysUserDTO userDTO);

    /**
     * 校验用户名称是否唯一
     * 
     * @param loginName 登录名称
     * @return 结果
     */
    public String checkLoginNameUnique(String loginName);

    /**
     * 校验手机号码是否唯一
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public String checkPhoneUnique(SysUserDTO userDTO);

    /**
     * 校验email是否唯一
     *
     * @param userDTO 用户信息
     * @return 结果
     */
    public String checkEmailUnique(SysUserDTO userDTO);

    /**
     * 校验用户是否允许操作
     * 
     * @param userDTO 用户信息
     */
    public void checkUserAllowed(SysUserDTO userDTO);

    /**
     * 根据用户ID查询用户所属角色组
     * 
     * @param user 用户ID
     * @return 结果
     */
    public String selectUserRoleGroup(SysUserVO user);

    /**
     * 根据用户ID查询用户所属岗位组
     * 
     * @param userId 用户ID
     * @return 结果
     */
    public String selectUserPostGroup(String userId);

    /**
     * 导入用户数据
     * 
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<SysUserDTO> userList, Boolean isUpdateSupport, String operName);

    /**
     * 用户状态修改
     * 
     * @param userDTO 用户信息
     * @return 结果
     */
    public int changeStatus(SysUserDTO userDTO);
}
