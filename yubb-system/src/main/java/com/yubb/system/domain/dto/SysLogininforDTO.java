package com.yubb.system.domain.dto;

import com.yubb.common.core.domain.saas.base.BaseDTO;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 *@Description 系统访问记录DTO
 *@Author zhushuyong
 *@Date 2021/6/19 21:38
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysLogininforDTO extends BaseDTO {
    private static final long serialVersionUID = 1L;

    /** 用户账号 */
    private String loginName;

    /** 登录状态 0成功 1失败 */
    private String status;

    /** 登录IP地址 */
    private String ipaddr;

    /** 登录地点 */
    private String loginLocation;

    /** 浏览器类型 */
    private String browser;

    /** 操作系统 */
    private String os;

    /** 提示消息 */
    private String msg;

    private String tenantId;

    /** 访问时间 */
    @Builder.Default
    private Date loginTime = new Date();

}