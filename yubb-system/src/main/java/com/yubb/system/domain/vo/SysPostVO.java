package com.yubb.system.domain.vo;

import com.yubb.common.annotation.Excel;
import com.yubb.common.annotation.Excel.ColumnType;
import com.yubb.common.core.domain.saas.base.BaseVO;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 *@Description 岗位web页面vo
 *@Author zhushuyong
 *@Date 2021/6/1 9:40
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysPostVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /** 岗位序号 */
    @Excel(name = "岗位序号", cellType = ColumnType.STRING)
    private String id;

    /** 岗位编码 */
    @Excel(name = "岗位编码")
    private String postCode;

    /** 岗位名称 */
    @Excel(name = "岗位名称")
    private String postName;

    /** 岗位排序 */
    @Excel(name = "岗位排序", cellType = ColumnType.NUMERIC)
    private String postSort;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 用户是否存在此岗位标识 默认不存在 */
    @Builder.Default
    private boolean flag = false;

}
