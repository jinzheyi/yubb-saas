package com.yubb.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.system.domain.SysPost;
import com.yubb.system.domain.vo.SysPostVO;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 岗位信息 数据层
 *@Author zhushuyong
 *@Date 2021/5/27 12:21
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysPostMapper extends BaseMapper<SysPost> {

    /**
     * 根据用户ID查询岗位
     * 
     * @param userId 用户ID
     * @return 岗位列表
     */
    public List<SysPostVO> selectPostsByUserId(String userId);

}
