package com.yubb.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.system.domain.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 用户与角色关联表 数据层
 *@Author zhushuyong
 *@Date 2021/6/4 21:41
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
