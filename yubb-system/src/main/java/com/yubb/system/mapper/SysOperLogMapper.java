package com.yubb.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.system.domain.SysOperLog;
import com.yubb.system.domain.dto.SysOperLogDTO;
import com.yubb.system.domain.vo.SysOperLogVO;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 操作日志 数据层
 *@Author zhushuyong
 *@Date 2021/6/20 20:50
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {

    /**
     * 查询系统操作日志集合
     * 
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    public List<SysOperLogVO> selectOperLogList(SysOperLogDTO operLog);
    
    /**
     * 清空操作日志
     */
    public void cleanOperLog();

}
