package com.yubb.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.system.domain.SysLogininfor;
import com.yubb.system.domain.dto.SysLogininforDTO;
import com.yubb.system.domain.vo.SysLogininforVO;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 系统访问日志情况信息 数据层
 *@Author zhushuyong
 *@Date 2021/6/17 14:16
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysLogininforMapper extends BaseMapper<SysLogininfor> {

    /**
     * 查询系统登录日志集合
     * 
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    public List<SysLogininforVO> selectLogininforList(SysLogininforDTO logininfor);

    /**
     * 清空系统登录日志
     * 
     * @return 结果
     */
    public int cleanLogininfor();

    /**
     * 新增系统登录日志
     *
     * @param logininfor 访问日志对象
     */
    @InterceptorIgnore(tenantLine = "true")
    public void insertLogininfor(SysLogininforDTO logininfor);

}
