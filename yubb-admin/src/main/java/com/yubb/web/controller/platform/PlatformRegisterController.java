package com.yubb.web.controller.platform;

import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.platform.dto.PlatformUserDTO;
import com.yubb.framework.shiro.service.SysRegisterService;
import com.yubb.platform.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *@Description 注册验证
 *@Author zhushuyong
 *@Date 2021/5/26 14:43
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
public class PlatformRegisterController extends BaseController
{
//    @Autowired
//    private SysRegisterService registerService;
//
//    @Autowired
//    private ISysConfigService configService;
//
//    @GetMapping("/register")
//    public String register()
//    {
//        return "register";
//    }
//
//    @PostMapping("/register")
//    @ResponseBody
//    public AjaxResult ajaxRegister(PlatformUserDTO user)
//    {
//        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser"))))
//        {
//            return error("当前系统没有开启注册功能！");
//        }
//        String msg = registerService.register(user);
//        return StringUtils.isEmpty(msg) ? success() : error(msg);
//    }
}
