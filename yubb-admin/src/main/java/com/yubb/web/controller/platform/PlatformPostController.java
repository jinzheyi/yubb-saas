package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.poi.ExcelUtil;
import com.yubb.platform.domain.dto.PlatformPostDTO;
import com.yubb.platform.domain.vo.PlatformPostVO;
import com.yubb.platform.service.IPlatformPostService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 岗位信息操作处理
 *@Author zhushuyong
 *@Date 2021/5/26 14:43
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/post")
public class PlatformPostController extends BaseController {
    private String prefix = "platform/post";

    @Autowired
    private IPlatformPostService postService;

    @RequiresPermissions("platform:post:view")
    @GetMapping()
    public String operlog() {
        return prefix + "/post";
    }

    @RequiresPermissions("platform:post:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo<PlatformPostVO> list(PlatformPostDTO postDTO) {
        return postService.selectPostPage(postDTO);
    }

    @PlatformLog(title = "岗位管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("platform:post:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlatformPostDTO postDTO) {
        List<PlatformPostVO> list = postService.selectPostList(postDTO);
        ExcelUtil<PlatformPostVO> util = new ExcelUtil<PlatformPostVO>(PlatformPostVO.class);
        return util.exportExcel(list, "岗位数据");
    }

    @RequiresPermissions("platform:post:remove")
    @PlatformLog(title = "岗位管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        try {
            return toAjax(postService.deletePostByIds(ids));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * 新增岗位
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存岗位
     */
    @RequiresPermissions("platform:post:add")
    @PlatformLog(title = "岗位管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated PlatformPostDTO postDTO) {
        if (UserConstants.POST_NAME_NOT_UNIQUE.equals(postService.checkPostNameUnique(postDTO))) {
            return error("新增岗位'" + postDTO.getPostName() + "'失败，岗位名称已存在");
        } else if (UserConstants.POST_CODE_NOT_UNIQUE.equals(postService.checkPostCodeUnique(postDTO))) {
            return error("新增岗位'" + postDTO.getPostName() + "'失败，岗位编码已存在");
        }
        postDTO.setCreateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(postService.insertPost(postDTO));
    }

    /**
     * 修改岗位
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("post", postService.selectPostById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存岗位
     */
    @RequiresPermissions("platform:post:edit")
    @PlatformLog(title = "岗位管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated PlatformPostDTO postDTO) {
        if (UserConstants.POST_NAME_NOT_UNIQUE.equals(postService.checkPostNameUnique(postDTO))) {
            return error("修改岗位'" + postDTO.getPostName() + "'失败，岗位名称已存在");
        } else if (UserConstants.POST_CODE_NOT_UNIQUE.equals(postService.checkPostCodeUnique(postDTO))) {
            return error("修改岗位'" + postDTO.getPostName() + "'失败，岗位编码已存在");
        }
        postDTO.setUpdateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(postService.updatePost(postDTO));
    }

    /**
     * 校验岗位名称
     */
    @PostMapping("/checkPostNameUnique")
    @ResponseBody
    public String checkPostNameUnique(PlatformPostDTO postDTO) {
        return postService.checkPostNameUnique(postDTO);
    }

    /**
     * 校验岗位编码
     */
    @PostMapping("/checkPostCodeUnique")
    @ResponseBody
    public String checkPostCodeUnique(PlatformPostDTO postDTO) {
        return postService.checkPostCodeUnique(postDTO);
    }
}
