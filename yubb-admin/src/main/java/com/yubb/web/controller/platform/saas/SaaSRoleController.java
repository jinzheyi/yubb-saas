package com.yubb.web.controller.platform.saas;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.platform.PlatformUser;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.vo.SysRoleVO;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.common.utils.poi.ExcelUtil;
import com.yubb.framework.shiro.util.AuthorizationUtils;
import com.yubb.platform.service.saas.ISaaSRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 平台管理租户角色信息
 *@Author zhushuyong
 *@Date 2021/5/26 14:44
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/saasRole")
public class SaaSRoleController extends BaseController
{
    private String prefix = "platform/saas/role";

    @Autowired
    private ISaaSRoleService saaSRoleService;

    @RequiresPermissions("platform:saasRole:view")
    @GetMapping()
    public String role()
    {
        return prefix + "/role";
    }

    @RequiresPermissions("platform:saasRole:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysRoleDTO role) {
        startPage();
        List<SysRoleVO> list = saaSRoleService.selectRoleList(role);
        return getDataTable(list);
    }

    @PlatformLog(title = "租户角色管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("platform:saasRole:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysRoleDTO role) {
        List<SysRoleVO> list = saaSRoleService.selectRoleList(role);
        ExcelUtil<SysRoleVO> util = new ExcelUtil<SysRoleVO>(SysRoleVO.class);
        return util.exportExcel(list, "角色数据");
    }

    /**
     * 新增角色
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存角色
     */
    @RequiresPermissions("platform:saasRole:add")
    @PlatformLog(title = "租户角色管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated SysRoleDTO role) {
        role.setCreateBy(ShiroUtils.getPlatformUser().getLoginName());
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(saaSRoleService.insertRole(role));

    }

    /**
     * 修改角色
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("role", saaSRoleService.selectRoleById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存角色
     */
    @RequiresPermissions("platform:saasRole:edit")
    @PlatformLog(title = "租户角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated SysRoleDTO role)
    {
        saaSRoleService.checkRoleAllowed(role, DozerUtils.copyProperties(ShiroUtils.getPlatformUser(), PlatformUser.class));
        role.setUpdateBy(ShiroUtils.getPlatformUser().getLoginName());
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(saaSRoleService.updateRole(role));
    }

    @RequiresPermissions("platform:saasRole:remove")
    @PlatformLog(title = "租户角色管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(saaSRoleService.deleteRoleByIds(ids, DozerUtils.copyProperties(ShiroUtils.getPlatformUser(), PlatformUser.class)));
    }

    /**
     * 角色状态修改
     */
    @PlatformLog(title = "租户角色管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("platform:saasRole:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(SysRoleDTO role) {
        saaSRoleService.checkRoleAllowed(role, DozerUtils.copyProperties(ShiroUtils.getPlatformUser(), PlatformUser.class));
        return toAjax(saaSRoleService.changeStatus(role));
    }

}