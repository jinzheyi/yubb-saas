package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.poi.ExcelUtil;
import com.yubb.framework.shiro.service.SysPasswordService;
import com.yubb.platform.domain.dto.PlatformLogininforDTO;
import com.yubb.platform.domain.vo.PlatformLogininforVO;
import com.yubb.platform.service.IPlatformLogininforService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *@Description 系统访问记录
 *@Author zhushuyong
 *@Date 2021/5/26 14:41
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/monitor/logininfor")
public class PlatformLogininforController extends BaseController
{
    private String prefix = "platform/monitor/logininfor";

    @Autowired
    private IPlatformLogininforService logininforService;

    @Autowired
    private SysPasswordService passwordService;

    @RequiresPermissions("platform:monitor:logininfor:view")
    @GetMapping()
    public String logininfor()
    {
        return prefix + "/logininfor";
    }

    @RequiresPermissions("platform:monitor:logininfor:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PlatformLogininforDTO logininfor)
    {
        startPage();
        List<PlatformLogininforVO> list = logininforService.selectLogininforList(logininfor);
        return getDataTable(list);
    }

    @PlatformLog(title = "登录日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("platform:monitor:logininfor:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlatformLogininforDTO logininfor)
    {
        List<PlatformLogininforVO> list = logininforService.selectLogininforList(logininfor);
        ExcelUtil<PlatformLogininforVO> util = new ExcelUtil<PlatformLogininforVO>(PlatformLogininforVO.class);
        return util.exportExcel(list, "登录日志");
    }

    @RequiresPermissions("platform:monitor:logininfor:remove")
    @PlatformLog(title = "登录日志", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(logininforService.deleteLogininforByIds(ids));
    }
    
    @RequiresPermissions("platform:monitor:logininfor:remove")
    @PlatformLog(title = "登录日志", businessType = BusinessType.CLEAN)
    @PostMapping("/clean")
    @ResponseBody
    public AjaxResult clean()
    {
        logininforService.cleanLogininfor();
        return success();
    }

    @RequiresPermissions("platform:monitor:logininfor:unlock")
    @PlatformLog(title = "账户解锁", businessType = BusinessType.OTHER)
    @PostMapping("/unlock")
    @ResponseBody
    public AjaxResult unlock(String loginName)
    {
        passwordService.clearPfLoginRecordCache(loginName);
        return success();
    }
}
