package com.yubb.framework.shiro.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yubb.common.constant.Constants;
import com.yubb.common.constant.ShiroConstants;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.domain.platform.PlatformTenant;
import com.yubb.common.core.domain.platform.PlatformUser;
import com.yubb.common.core.domain.platform.vo.PlatformUserVO;
import com.yubb.common.core.domain.saas.dto.SysUserDTO;
import com.yubb.common.core.domain.saas.vo.SysUserVO;
import com.yubb.common.enums.UserStatus;
import com.yubb.common.exception.user.*;
import com.yubb.common.utils.DateUtils;
import com.yubb.common.utils.MessageUtils;
import com.yubb.common.utils.ServletUtils;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.framework.manager.AsyncManager;
import com.yubb.framework.manager.factory.AsyncFactory;
import com.yubb.framework.shiro.realm.token.CustomToken;
import com.yubb.platform.service.IPlatformTenantService;
import com.yubb.platform.service.IPlatformUserService;
import com.yubb.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.Objects;

/**
 *@Description 登录校验方法
 *@Author zhushuyong
 *@Date 2021/6/24 17:27
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Component
public class SysLoginService
{
    @Autowired
    private SysPasswordService passwordService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private IPlatformUserService platformUserService;

    @Autowired
    private IPlatformTenantService platformTenantService;

    /**
     * 登录
     */
    public SysUserVO login(CustomToken upToken)
    {
        String tenantNo = upToken.getTenantNo();
        String username = upToken.getUsername();
        String password = String.valueOf(upToken.getPassword());

        //判断是否已有平台用户登录（同一浏览器登录两种不同的用户会有bug，这里进行检测）
        PlatformUserVO platformUserVO = ShiroUtils.getPlatformUser();
        if (!Objects.isNull(platformUserVO)) {
            throw new UserExistingException();
        }

        // 验证码校验
        if (ShiroConstants.CAPTCHA_ERROR.equals(ServletUtils.getRequest().getAttribute(ShiroConstants.CURRENT_CAPTCHA)))
        {
            //AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
        if (StringUtils.isEmpty(upToken.getTenantNo())){
            throw new TenantNoException();
        }
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            //AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("not.null")));
            throw new UserNotExistsException();
        }
        // 密码如果不在指定范围内 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            //AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }

        // 用户名不在指定范围内 错误
        if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            //AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }

        SysUserVO user = tenantValid(tenantNo, username);

        /**
        if (user == null && maybeMobilePhoneNumber(username))
        {
            user = userService.selectUserByPhoneNumber(username);
        }

        if (user == null && maybeEmail(username))
        {
            user = userService.selectUserByEmail(username);
        }
        */

        if (user == null)
        {
            //AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.not.exists")));
            throw new UserNotExistsException();
        }
        //初始化日志租户id
        ServletUtils.getSession().setAttribute(ShiroConstants.TENANT_ID, user.getTenantId());
        if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.delete")));
            throw new UserDeleteException();
        }
        
        if (UserStatus.DISABLE.getCode().equals(user.getStatus()))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.blocked", user.getRemark())));
            throw new UserBlockedException();
        }

        passwordService.validate(user, password);

        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        recordLoginInfo(user);
        return user;
    }

    /**
     * 平台用户登录
     */
    public PlatformUserVO platformLogin(CustomToken upToken)
    {
        String username = upToken.getUsername();
        String password = String.valueOf(upToken.getPassword());

        //判断是否已有租户用户登录（同一浏览器登录两种不同的用户会有bug，这里进行检测）
        SysUserVO sysUserVO = ShiroUtils.getSysUser();
        if (!Objects.isNull(sysUserVO) && StrUtil.isNotBlank(sysUserVO.getTenantId())) {
            throw new UserExistingException();
        }

        // 验证码校验
        if (ShiroConstants.CAPTCHA_ERROR.equals(ServletUtils.getRequest().getAttribute(ShiroConstants.CURRENT_CAPTCHA)))
        {
            AsyncManager.me().execute(AsyncFactory.recordPlatformLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            AsyncManager.me().execute(AsyncFactory.recordPlatformLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("not.null")));
            throw new UserNotExistsException();
        }
        // 密码如果不在指定范围内 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            AsyncManager.me().execute(AsyncFactory.recordPlatformLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }

        // 用户名不在指定范围内 错误
        if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            AsyncManager.me().execute(AsyncFactory.recordPlatformLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }

        // 查询用户信息
        PlatformUserVO user = platformUserService.selectUserByLoginName(username);

        /**
         if (user == null && maybeMobilePhoneNumber(username))
         {
         user = userService.selectUserByPhoneNumber(username);
         }

         if (user == null && maybeEmail(username))
         {
         user = userService.selectUserByEmail(username);
         }
         */

        if (user == null)
        {
            AsyncManager.me().execute(AsyncFactory.recordPlatformLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.not.exists")));
            throw new UserNotExistsException();
        }

        if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
        {
            AsyncManager.me().execute(AsyncFactory.recordPlatformLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.delete")));
            throw new UserDeleteException();
        }

        if (UserStatus.DISABLE.getCode().equals(user.getStatus()))
        {
            AsyncManager.me().execute(AsyncFactory.recordPlatformLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.blocked", user.getRemark())));
            throw new UserBlockedException();
        }

        passwordService.validate(user, password);

        AsyncManager.me().execute(AsyncFactory.recordPlatformLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        recordLoginInfo(user);
        return user;
    }

    /**
    private boolean maybeEmail(String username)
    {
        if (!username.matches(UserConstants.EMAIL_PATTERN))
        {
            return false;
        }
        return true;
    }

    private boolean maybeMobilePhoneNumber(String username)
    {
        if (!username.matches(UserConstants.MOBILE_PHONE_NUMBER_PATTERN))
        {
            return false;
        }
        return true;
    }
    */

    private SysUserVO tenantValid(String tenantNo, String username) {
        PlatformTenant platformTenant = null;
        try {
            platformTenant = platformTenantService.getOne(
                    Wrappers.query(PlatformTenant.builder().tenantNo(tenantNo).build()), true);
            if (platformTenant == null) {
                throw new TenantNoException();
            }
        } catch (Exception e) {
            throw new TenantNoException();
        }
        //初始化日志租户id
        ServletUtils.getSession().setAttribute(ShiroConstants.TENANT_ID, platformTenant.getId());
        //租户相关控制
        boolean effectiveDate = DateUtils.belongCalendar(new Date(), platformTenant.getStartDate(), platformTenant.getEndDate());
        if (!effectiveDate) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.tenant.date.range")));
            throw new UserTenantDateRangeException();
        }
        if (Constants.STATUS_ONE.equals(platformTenant.getStatus())){
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.tenant.status")));
            throw new UserTenantStatusException();
        }
        // 查询用户信息
        return userService.selectUserByLoginNameAndTenantId(username, platformTenant.getId());
    }

    /**
     * 记录登录信息
     */
    public void recordLoginInfo(SysUserVO user)
    {
        user.setLoginIp(ShiroUtils.getIp());
        user.setLoginDate(DateUtils.getNowDate());
        userService.updateUserInfo(DozerUtils.copyProperties(user, SysUserDTO.class));
    }

    /**
     * 记录登录信息
     */
    public void recordLoginInfo(PlatformUserVO user)
    {
        user.setLoginIp(ShiroUtils.getIp());
        user.setLoginDate(DateUtils.getNowDate());
        platformUserService.updateById(DozerUtils.copyProperties(user, PlatformUser.class));
    }

}
