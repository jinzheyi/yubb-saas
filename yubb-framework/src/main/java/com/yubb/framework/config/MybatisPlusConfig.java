package com.yubb.framework.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.yubb.framework.handler.YubbTenantLineHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *@Description Mybatis支持*匹配扫描包
 *@Author zhushuyong
 *@Date 2021/6/24 16:28
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Configuration
@MapperScan("com.yubb.*.mapper")
public class MybatisPlusConfig {

    @Autowired
    private YubbTenantLineHandler yubbTenantLineHandler;

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //租户拦截器
        TenantLineInnerInterceptor tenantSqlParser = new TenantLineInnerInterceptor();
        // 设置租户处理器
        tenantSqlParser.setTenantLineHandler(yubbTenantLineHandler);
        interceptor.addInnerInterceptor(tenantSqlParser);
        //分页拦截器
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        //单页分页条数限制(默认无限制,参见 插件#handlerLimit 方法)
        paginationInnerInterceptor.setMaxLimit(1000L);
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        return interceptor;
    }

}