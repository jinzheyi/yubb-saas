package com.yubb.common.core.domain.saas.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description 传入参数的基类
 * @Author zhushuyong
 * @Date 2021/5/27 21:34
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value="BaseDto对象",description="基础Dto继承类")
public class BaseDTO implements Serializable {

    private static final long serialVersionUID = 2994373247289506578L;

    /**
     * id
     */
    @ApiModelProperty(hidden = true)
    protected String id;

    /**
     * 创建者
     */
    @ApiModelProperty(value="创建者")
    protected String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间",example="yyyy-MM-dd HH:mm:ss")
    protected Date createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value="更新者")
    protected String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间",example="yyyy-MM-dd HH:mm:ss")
    protected Date updateTime;

    /** 备注 */
    protected String remark;

    /** 搜索值 */
    protected String searchValue;

    /** 请求参数 */
    protected Map<String, Object> params;

    public Map<String, Object> getParams() {
        if (params == null) {
            params = new HashMap<>();
        }
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

}
