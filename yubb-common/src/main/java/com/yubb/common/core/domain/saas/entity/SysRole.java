package com.yubb.common.core.domain.saas.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yubb.common.constant.Constants;
import com.yubb.common.core.domain.saas.base.BaseDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

/**
 *@Description 角色表
 *@Author zhushuyong
 *@Date 2021/5/30 23:17
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@TableName("sys_role")
public class SysRole extends BaseDO {

    private static final long serialVersionUID = 1L;

    /** 角色名称 */
    private String roleName;

    /** 角色权限 */
    private String roleKey;

    /** 角色排序 */
    private String roleSort;

    /** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限） */
    private String dataScope;

    /** 角色状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 备注 */
    private String remark;

    public boolean isAdmin() {
        return isAdmin(getRoleKey());
    }

    public static boolean isAdmin(String roleKey) {
        return StringUtils.isNotBlank(roleKey) && Constants.SAAS_ADMIN.equals(roleKey);
    }

}
