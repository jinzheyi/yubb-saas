package com.yubb.common.core.domain.saas.vo;

import com.yubb.common.annotation.Excel;
import com.yubb.common.annotation.Excel.ColumnType;
import com.yubb.common.core.domain.saas.base.BaseVO;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 *@Description 角色vo
 *@Author zhushuyong
 *@Date 2021/6/9 11:41
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysRoleVO extends BaseVO {
    private static final long serialVersionUID = 1L;

    /** 角色ID */
    @Excel(name = "角色序号", cellType = ColumnType.STRING)
    private String id;

    /** 角色名称 */
    @Excel(name = "角色名称")
    private String roleName;

    /** 角色权限 */
    @Excel(name = "角色权限")
    private String roleKey;

    /** 角色排序 */
    @Excel(name = "角色排序", cellType = ColumnType.STRING)
    private String roleSort;

    /** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限） */
    @Excel(name = "数据范围", readConverterExp = "1=所有数据权限,2=自定义数据权限,3=本部门数据权限,4=本部门及以下数据权限")
    private String dataScope;

    /** 角色状态（0正常 1停用） */
    @Excel(name = "角色状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 用户是否存在此角色标识 默认不存在 */
    @Builder.Default
    private boolean flag = false;

    /** 菜单组 */
    private String[] menuIds;

    /** 部门组（数据权限） */
    private String[] deptIds;

}
