package com.yubb.common.core.domain.saas.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yubb.common.annotation.Excel;
import com.yubb.common.annotation.Excel.ColumnType;
import com.yubb.common.annotation.Excel.Type;
import com.yubb.common.annotation.Excels;
import com.yubb.common.core.domain.saas.base.BaseVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import java.util.Date;
import java.util.List;

/**
 *@Description 用户web页面vo
 *@Author zhushuyong
 *@Date 2021/6/1 9:41
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysUserVO extends BaseVO {
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @Excel(name = "用户序号", cellType = ColumnType.STRING, prompt = "用户编号")
    private String id;

    /** 部门ID */
    @Excel(name = "部门编号", type = Type.IMPORT)
    private String deptId;

    /** 部门父ID */
    private String parentId;

    /** 角色ID */
    private String roleId;

    /** 登录名称 */
    @Excel(name = "登录名称")
    private String loginName;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 用户类型 */
    private String userType;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    private String email;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phonenumber;

    /** 用户性别 */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    @JsonIgnore
    private String password;

    /** 盐加密 */
    private String salt;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    @Excel(name = "最后登录IP", type = Type.EXPORT)
    private String loginIp;

    /** 最后登录时间 */
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    private Date loginDate;

    /** 密码最后更新时间 */
    private Date pwdUpdateDate;

    /** 部门对象 */
    @Excels({
            @Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT),
            @Excel(name = "部门负责人", targetAttr = "leader", type = Type.EXPORT)
    })
    private SysDeptVO dept;

    private List<SysRoleVO> roles;

    /** 角色组 */
    private String[] roleIds;

    /** 岗位组 */
    private String[] postIds;

    public SysDeptVO getDept() {
        if (dept == null) {
            dept = new SysDeptVO();
        }
        return dept;
    }

    public void setDept(SysDeptVO dept) {
        this.dept = dept;
    }

    public List<SysRoleVO> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRoleVO> roles) {
        this.roles = roles;
    }

}
