package com.yubb.common.core.domain.platform.dto;

import com.yubb.common.core.domain.platform.base.PlatformDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 租户对象DTO
 * 
 * @author zhushuyong
 * @date 2021-07-02
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PlatformTenantDTO extends PlatformDTO {
    private static final long serialVersionUID = 1L;

    /** 租户名称 */
    @NotBlank(message = "租户名称不能为空")
    @Size(min = 5, max = 30, message = "租户名称长度不能超过30个字符")
    private String tenantName;

    /** 租户编号 */
    @NotBlank(message = "租户编号不能为空")
    @Size(min = 10, max = 32, message = "租户编号长度不能超过32个字符")
    private String tenantNo;

    /**
     * 租户logo
     */
    private String logoUrl;

    /** 租户状态（0正常 1停用） */
    private String status;

    /** 租户系统开始时间 */
    @NotNull(message = "租户系统开始时间不能为空")
    private Date startDate;

    /** 租户系统到期时间 */
    @NotNull(message = "租户系统到期时间不能为空")
    private Date endDate;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 部门id
     */
    private String deptId;

    /** 用户名称 */
    @NotBlank(message = "用户昵称不能为空")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    private String userName;

    /** 部门名称 */
    @NotBlank(message = "顶级部门名称不能为空")
    @Size(min = 0, max = 30, message = "顶级部门名称长度不能超过30个字符")
    private String deptName;

    /** 登录名称 */
    @NotBlank(message = "登录账号不能为空")
    @Size(min = 0, max = 30, message = "登录账号长度不能超过30个字符")
    private String loginName;

    /** 用户邮箱 */
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    private String email;

    /** 手机号码 */
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    private String phonenumber;

    /** 用户性别 */
    private String sex;

    /** 密码 */
    private String password;

    /** 盐加密 */
    private String salt;

    /** 用户帐号状态（0正常 1停用） */
    private String userStatus;

}
