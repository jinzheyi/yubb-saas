package com.yubb.common.core.domain.saas.dto;

import com.yubb.common.core.domain.saas.base.BaseDTO;
import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @Description 用户对象
 * @Author zhushuyong
 * @Date 2021/6/1 23:22
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysUserDTO extends BaseDTO {
    private static final long serialVersionUID = 1L;

    /** 部门ID */
    private String deptId;

    /** 部门父ID */
    private String parentId;

    /** 角色ID */
    private String roleId;

    /** 登录名称 */
    @NotBlank(message = "登录账号不能为空")
    @Size(min = 0, max = 30, message = "登录账号长度不能超过30个字符")
    private String loginName;

    /** 用户名称 */
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    private String userName;

    /** 用户类型 */
    private String userType;

    /** 用户邮箱 */
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    private String email;

    /** 手机号码 */
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    private String phonenumber;

    /** 用户性别 */
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 盐加密 */
    private String salt;

    /** 帐号状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    private String loginIp;

    /** 最后登录时间 */
    private Date loginDate;

    /** 密码最后更新时间 */
    private Date pwdUpdateDate;

    /** 角色组 */
    private String[] roleIds;

    /** 岗位组 */
    private String[] postIds;

}
